---
title: "Create:Code Creation Project List"
description: A list of current and past projects for the Create:Code Creation team
---

### Active Projects

| Start Date | Project | Description | Tech Lead |
|------------|---------|-------------|-----------|
| 2023-11-01 | [Repository X-Ray](https://gitlab.com/groups/gitlab-org/-/epics/11733) | Gather information about a GitLab repository to provide extra context when generating code suggestions | Mikołaj  |
| 2024-01-17 | [Improve Architecture to Decrease Latency](https://gitlab.com/groups/gitlab-org/-/epics/12224) | Rethink the Code Suggestions architecture in an effort to decrease latency | Jan  |
| 2024-02-12 | [Improve Trigger Logic](https://gitlab.com/groups/gitlab-org/-/epics/12100) | How can we optimize when we request code suggestions from the LLMs | Sri |


### Archived Projects 

| Start Date | End Date | Project | Description | Tech Lead |
|------------|----------|---------|-------------|-----------|
|            |          |         |             |           |
|            |          |         |             |           |
